package com.maxeoneo.gameoflife

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}