pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

rootProject.name = "GameOfLife"
include(":androidApp")
include(":jvmApp")
include(":shared")
include(":compose")
