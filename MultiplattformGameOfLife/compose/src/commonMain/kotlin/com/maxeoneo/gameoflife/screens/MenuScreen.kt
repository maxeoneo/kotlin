package com.maxeoneo.gameoflife.screens

import androidx.compose.runtime.Composable


@Composable
fun MenuScreen() {
  App()
}

@Composable
@Preview
fun App() {
  var text by remember { mutableStateOf(Greeting().greeting()) }

  MaterialTheme {
    Button(onClick = {
      text = "Hello, Desktop!"
    }) {
      Text(text)
    }
  }
}