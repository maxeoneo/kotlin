plugins {
  id("com.android.application")
  kotlin("android")
}

dependencies {
  implementation(project(":shared"))
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.10")
  implementation("com.google.android.material:material:1.4.0")
  implementation("androidx.appcompat:appcompat:1.3.1")
  implementation("androidx.constraintlayout:constraintlayout:2.1.1")

  // COMPOSE DEPENDENCIES HERE
  implementation("androidx.compose.runtime:runtime:1.1.0-rc03")
  implementation("androidx.compose.ui:ui:1.1.0-rc03")
  implementation("androidx.compose.foundation:foundation-layout:1.1.0-rc03")
  implementation("androidx.compose.material:material:1.1.0-rc03")
  implementation("androidx.compose.material:material-icons-extended:1.1.0-rc03")
  implementation("androidx.compose.foundation:foundation:1.1.0-rc03")
  implementation("androidx.compose.ui:ui-tooling:1.1.0-rc03")

  implementation("androidx.activity:activity-compose:1.3.0-alpha05")
  implementation("androidx.compose.ui:ui-tooling-preview:1.0.5")
}

android {
  compileSdk = 31
  defaultConfig {
    applicationId = "com.maxeoneo.gameoflife"
    minSdk = 23
    targetSdk = 31
    versionCode = 1
    versionName = "1.0"
  }
  buildTypes {
    getByName("release") {
      isMinifyEnabled = false
    }
  }

//  kotlinOptions {
//    jvmTarget = JavaVersion.VERSION_1_8.toString()
//  }

  buildFeatures {
    compose = true
  }

  composeOptions {
    kotlinCompilerExtensionVersion = "1.1.0-rc03"
  }
}