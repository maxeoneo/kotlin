package gol.bl

import kotlin.test.Test
import kotlin.test.*

internal class BoardTest {

  @Test
  fun testRowFromIndex() {
    // given
    val board = Board(BoardSize(2, 3))

    // when & then
    assertEquals(0, board.rowFrom(0))
    assertEquals(0, board.rowFrom(1))
    assertEquals(0, board.rowFrom(2))
    assertEquals(1, board.rowFrom(3))
    assertEquals(1, board.rowFrom(4))
    assertEquals(1, board.rowFrom(5))
  }

  @Test
  fun testColumnFromIndex() {
    // given
    val board = Board(BoardSize(2, 3))

    // when & then
    assertEquals(0, board.columnFrom(0))
    assertEquals(1, board.columnFrom(1))
    assertEquals(2, board.columnFrom(2))
    assertEquals(0, board.columnFrom(3))
    assertEquals(1, board.columnFrom(4))
    assertEquals(2, board.columnFrom(5))
  }

  @Test
  fun testIndexFromRowAndColumn() {
    // given
    val board = Board(BoardSize(2, 3))

    // when & then
    assertEquals(0, board.indexFrom(0, 0))
    assertEquals(1, board.indexFrom(0, 1))
    assertEquals(2, board.indexFrom(0, 2))
    assertEquals(3, board.indexFrom(1, 0))
    assertEquals(4, board.indexFrom(1, 1))
    assertEquals(5, board.indexFrom(1, 2))
  }

  @Test
  fun testInitializeBoardNumberOfNeighbours() {
    // given
    val board = Board(BoardSize(3, 3))

    // when
    board.initialize()

    // then
    assertEquals(3, board.fields[0].neighbours.size)
    assertEquals(5, board.fields[1].neighbours.size)
    assertEquals(3, board.fields[2].neighbours.size)
    assertEquals(5, board.fields[3].neighbours.size)
    assertEquals(8, board.fields[4].neighbours.size)
    assertEquals(5, board.fields[5].neighbours.size)
    assertEquals(3, board.fields[6].neighbours.size)
    assertEquals(5, board.fields[7].neighbours.size)
    assertEquals(3, board.fields[8].neighbours.size)
  }

  @Test
  fun testInitializeBoardPositionOfNeighbours() {
    // given
    val board = Board(BoardSize(3, 3))

    // when
    board.initialize()

    // then
    assertEquals(1,board.fields[4].neighbours[0].row)
    assertEquals(0,board.fields[4].neighbours[0].column)

    assertEquals(0,board.fields[4].neighbours[1].row)
    assertEquals(0,board.fields[4].neighbours[1].column)

    assertEquals(2,board.fields[4].neighbours[2].row)
    assertEquals(0,board.fields[4].neighbours[2].column)

    assertEquals(1,board.fields[4].neighbours[3].row)
    assertEquals(2,board.fields[4].neighbours[3].column)

    assertEquals(0,board.fields[4].neighbours[4].row)
    assertEquals(2,board.fields[4].neighbours[4].column)

    assertEquals(2,board.fields[4].neighbours[5].row)
    assertEquals(2,board.fields[4].neighbours[5].column)

    assertEquals(0,board.fields[4].neighbours[6].row)
    assertEquals(1,board.fields[4].neighbours[6].column)

    assertEquals(2,board.fields[4].neighbours[7].row)
    assertEquals(1,board.fields[4].neighbours[7].column)
  }

  @Test
  fun testRefreshingOfStates() {
    // given
    val board = Board(BoardSize(3, 1))
    board.initialize()

    board.fields[0].active = true
    board.fields[1].active = true
    board.fields[2].active = true

    // when
    board.refresh()

    // then
    assertFalse(board.fields[0].active)
    assertTrue(board.fields[1].active)
    assertFalse(board.fields[2].active)
  }
}