package gol.bl

import org.junit.Assert.*
import org.junit.Test

internal class FieldTest {

    fun createNeighbours(activeNeighbours: Int): List<Field> {
        return List(8) { Field(it < activeNeighbours) }
    }

    @Test
    fun deadFieldShouldStayDead() {
        // given
        val field = Field(0, 0)
        field.neighbours = createNeighbours(2)

        // when
        field.refresh()

        // then
        assertFalse(field.active)
    }

    @Test
    fun aliveFieldShouldDieOfLoneliness() {
        // given
        val field = Field(0, 0)
        field.neighbours = createNeighbours(1)
        field.active = true

        // when
        field.refresh()

        // then
        assertFalse(field.active)
    }

    @Test
    fun aliveFieldShouldStayAlive() {
        // given
        val field = Field(0, 0)
        field.neighbours = createNeighbours(2)
        field.active = true

        // when
        field.refresh()

        // then
        assertTrue(field.active)
    }

    @Test
    fun deadFieldShouldAwake() {
        // given
        val field = Field(0, 0)
        field.neighbours = createNeighbours(3)

        // when
        field.refresh()

        // then
        assertTrue(field.active)
    }

    @Test
    fun aliveFieldShouldDieOfOverpopulation() {
        // given
        val field = Field(0, 0)
        field.neighbours = createNeighbours(4)
        field.active = true

        // when
        field.refresh()

        // then
        assertFalse(field.active)
    }
}