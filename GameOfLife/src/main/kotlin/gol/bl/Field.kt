package gol.bl

class Field(var row: Int = 0, var column: Int = 0) {

  var active: Boolean = false
  var neighbours: List<Field> = emptyList()
    set(value) {
      field = value
      updateActiveNeighbours()
    }

  var activeNeighbours: Int = 0

  constructor(active: Boolean = false) : this(0,0) {
    this.active = active
  }

  fun refresh() {
    if (isLonely() || isOverpopulated()) {
      die()
    } else if (hasPerfectEnvironment()) {
      awake()
    }
  }
  
  fun updateActiveNeighbours() {
    activeNeighbours = neighbours.filter { it.active }.size
  }

  private fun isLonely() = activeNeighbours < 2

  private fun isOverpopulated() = activeNeighbours > 3

  private fun hasPerfectEnvironment() = activeNeighbours == 3

  private fun die() { active = false }

  private fun awake() { active = true }
}