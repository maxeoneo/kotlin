package gol.bl

import java.lang.Integer.max

class Board(val size: BoardSize) {
  val fields: List<Field> = List(size.rows * size.columns) { Field( rowFrom(it), columnFrom(it)) }

  fun refresh() {
    for (field in fields) {
      field.updateActiveNeighbours()
    }
    for (field in fields) {
      field.refresh()
    }
  }

  fun initialize() {
    var index = 0
    for (field in fields) {
      field.neighbours = getNeighboursFrom(index)
      ++index
    }
  }

  fun getNeighboursFrom(index: Int): List<Field> {
    val neighbours = MutableList<Field>(0) { Field() }

    neighbours.addAll(leftNeighbours(index))
    neighbours.addAll(rightNeighbours(index))

    if (hasTopNeighbours(index)) {
      neighbours.add(fields[indexFrom(rowFrom(index) - 1, columnFrom(index))])
    }

    if (hasBottomNeighbours(index)) {
      neighbours.add(fields[indexFrom(rowFrom(index) + 1, columnFrom(index))])
    }

    return neighbours.toList()
  }

  private fun leftNeighbours(index: Int): MutableList<Field> {
    val neighbours = MutableList<Field>(0) { Field() }
    if (hasLeftNeighbours(index)) {
      neighbours.add(fields[indexFrom(rowFrom(index), columnFrom(index) - 1)])
      if (hasTopNeighbours(index)) {
        neighbours.add(fields[indexFrom(rowFrom(index) - 1, columnFrom(index) - 1)])
      }
      if (hasBottomNeighbours(index)) {
        neighbours.add(fields[indexFrom(rowFrom(index) + 1, columnFrom(index) - 1)])
      }
    }
    return neighbours
  }

  private fun rightNeighbours(index: Int): MutableList<Field> {
    val neighbours = MutableList<Field>(0) { Field() }
    if (hasRightNeighbours(index)) {
      neighbours.add(fields[indexFrom(rowFrom(index), columnFrom(index) + 1)])
      if (hasTopNeighbours(index)) {
        neighbours.add(fields[indexFrom(rowFrom(index) - 1, columnFrom(index) + 1)])
      }
      if (hasBottomNeighbours(index)) {
        neighbours.add(fields[indexFrom(rowFrom(index) + 1, columnFrom(index) + 1)])
      }
    }
    return neighbours
  }

  private fun hasLeftNeighbours(index: Int) = columnFrom(index) > 0
  private fun hasRightNeighbours(index: Int) = columnFrom(index) < size.columns - 1
  private fun hasTopNeighbours(index: Int) = rowFrom(index) > 0
  private fun hasBottomNeighbours(index: Int) = rowFrom(index) < size.rows - 1

  private fun topLeftNeighbour(index: Int): Field {
    val row = max(rowFrom(index) - 1, 0)
    val column = max(columnFrom(index) - 1, 0)
    return fields[indexFrom(row, column)]
  }

  fun rowFrom(index: Int): Int {
    if (size.columns == 0) {
      return 0
    }
    return index / size.columns
  }

  fun columnFrom(index: Int): Int {
    if (size.columns == 0) {
      return 0
    }
    return index % size.columns
  }

  fun indexFrom(row: Int, column: Int): Int {
    return row * size.columns + column
  }
}

