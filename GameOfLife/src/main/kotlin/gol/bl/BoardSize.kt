package gol.bl

data class BoardSize(val rows: Int, val columns: Int)
