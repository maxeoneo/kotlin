package gol.ui

import gol.bl.BoardSize
import gol.bl.Field

interface GameUi {
  fun setBoardSize(newSize: BoardSize)
  fun setFields(newFields: List<Field>)
  fun refresh()
}