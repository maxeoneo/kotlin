package gol.ui

import gol.bl.BoardSize
import gol.bl.Field

class ConsoleUi() : GameUi {
  private var size = BoardSize(0,0)
  private var fields = emptyList<Field>()

  override fun setBoardSize(newSize: BoardSize) {
    size = newSize
  }

  override fun setFields(newFields: List<Field>) {
    fields = newFields
  }

  override fun refresh() {
    val builder = StringBuilder()
    for (counter in 0..size.columns) {
      builder.append("#")
    }

    println(builder.toString())
    for (row in 0..size.rows) {
      printRow(row)
      println()
    }

    println(builder.toString())
  }

  fun printRow(row: Int) {
    val currentRow = MutableList<Field>(0) { Field() }
    for (field in fields) {
      if (field.row == row) {
        currentRow.add(field.column, field)
      }
    }

    for (field in currentRow) {
      if (field.active) {
        print("X")
      } else {
        print(" ")
      }
    }
  }
}