package gol.game

import gol.bl.Board
import gol.bl.BoardSize
import gol.ui.ConsoleUi
import gol.ui.GameUi

class Game(size: BoardSize, val ui: GameUi) {
  val board = Board(size)
  var playing = false

  init {
    board.initialize()
    ui.setBoardSize(size)
    ui.setFields(board.fields)
  }



  fun startPlaying() {
    playing = true
    while (playing) {
      Thread.sleep(1000)
      board.refresh()
      ui.refresh()
    }
  }

  fun stopPlaying() {
    playing = false
  }
}

fun main() {
  val ui = ConsoleUi()
  val game = Game(BoardSize(5,5), ui)
  game.board.fields[7].active = true
  game.board.fields[12].active = true
  game.board.fields[17].active = true

  for (counter in 0..5) {
    Thread.sleep(1000)
    game.board.refresh()
    ui.refresh()
  }
}